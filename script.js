/**
 * Реализовать функцию, которая позволит оценить, успеет ли команда разработчиков сдать проект до наступления
 * дедлайна.

 Технические требования:

 Функция на вход принимает три параметра:

 *массив из чисел, который представляет скорость работы разных членов команды. Количество элементов в массиве
 означает количество человек в команде. Каждый элемент означает сколько стори поинтов (условная оценка сложности
 выполнения задачи) может выполнить данный разработчик за 1 день.
 *массив из чисел, который представляет собой беклог (список всех задач, которые необходимо выполнить). Количество
 элементов в массиве означает количество задач в беклоге. Каждое число в массиве означает количество стори поинтов,
 необходимых для выполнения данной задачи.
 *дата дедлайна (объект типа Date).


 После выполнения, функция должна посчитать, успеет ли команда разработчиков выполнить все задачи из беклога до
 наступления дедлайна (работа ведется начиная с сегодняшнего дня). Если да, вывести на экран сообщение: Все задачи
 будут успешно выполнены за ? дней до наступления дедлайна!. Подставить нужное число дней в текст. Если нет,
 вывести сообщение Команде разработчиков придется потратить дополнительно ? часов после дедлайна, чтобы выполнить
 все задачи в беклоге

 Работа идет по 8 часов в день по будним дням
 * */
const teamSpeed = [3, 8, 10, 4, 5];
const backlogArray = [31, 15, 82, 12, 45, 84, 20, 25, 34, 212, 132, 63, 44, 200];
const date = new Date(2020, 2, 4);

function estimateDaysAmount(teamSpeed, backlog, endDate) {

    /*count the amount of working days to deadline*/

    const currentDate = new Date();
    let workingDays = 0;

    while (currentDate <= endDate) {
        let weekDay = currentDate.getDay();

        if (weekDay !== 0 && weekDay !== 6)
            workingDays++;
        currentDate.setDate(currentDate.getDate() + 1);
    }

/*count total story points the team is able to do per day*/
    let pointsSum = 0;

    teamSpeed.forEach((item) => {
        pointsSum += item;
    });

/*count total story points are needed to finish the project*/
    const teamTotalPoints = pointsSum * workingDays;
    let backlogTotalPoints = 0;
    backlog.forEach((item) => {
        backlogTotalPoints += item;
    });

    /*count difference between the team speed and deadline*/
    if (backlogTotalPoints > teamTotalPoints) {
        let diff = backlogTotalPoints - teamTotalPoints;
        let days = Math.floor(diff / pointsSum);
        alert(`The team will need ${days} days in addition to do all tasks`);
    } else {
        let diff = teamTotalPoints - backlogTotalPoints;
        let days = Math.floor(diff / pointsSum);
        alert(`All tasks will be done in ${days} days to deadline`);
    }
}

estimateDaysAmount(teamSpeed, backlogArray, date);